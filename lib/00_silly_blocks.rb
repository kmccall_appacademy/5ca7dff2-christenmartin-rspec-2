def reverser
  words = yield
  words.split(" ").map {|word| word.split("").reverse.join("")}.join(" ")
end


def adder(num=1)
  num2 = yield
  num2 + num
end

def repeater(num=1)
  num.times {yield}
end
