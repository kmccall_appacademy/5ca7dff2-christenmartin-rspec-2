def measure(n=1)
  first_time = Time.now
  if block_given?
    n.times {yield}
  end
  total_elapsed= Time.now - first_time
  total_elapsed / n
end
